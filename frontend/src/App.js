import React, { Component } from 'react';
import MainPage from './Component/MainPage';
import './App.css';

export class App extends Component {
  render() {
    return (
      <div className='App'>
        <MainPage />
      </div>
    );
  }
}

export default App;
