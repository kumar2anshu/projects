import React, { Component } from 'react';
//import { Jumbotron } from 'reactstrap';
import axios from 'axios';
import '../App.css';

class Course extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courseClass: '',
      courseName: '',
      courseDuration: '',
      mentor: ''
    };
  }
  changeClassHandler = e => {
    this.setState({
      courseClass: e.target.value
    });
  };
  courseNameChangeHandler = e => {
    this.setState({
      courseName: e.target.value
    });
  };
  courseDurationChangeHandler = e => {
    this.setState({
      courseDuration: e.target.value
    });
  };
  changeMentorHandler = e => {
    this.setState({
      mentor: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    const data = {
      courseClass: this.state.courseClass,
      courseName: this.state.courseName,
      courseDuration: this.state.courseDuration,
      mentor: this.state.mentor
    };

    axios.post('http://localhost:5000/api/courses', data).then(res => {
      console.log(res);
      if (res.statusText === 'OK') {
        alert('Thank You!! Your course is successfully added');
      }
    });
  };

  render() {
    return (
      <>
        <center>
          <form onSubmit={this.handleSubmit} className='courseForm'>
            <div>
              <center>
                <h2>Enter Course Details</h2>
              </center>
            </div>
            <div className='courseClass'>
              <label htmlFor='courseClass'>courseClass* :</label>
              <input
                type='Number'
                placeholder='Choose the class for which this course is designed'
                name='courseClass'
                min='1'
                max='12'
                value={this.state.courseClass}
                onChange={this.changeClassHandler}
              />
            </div>
            <div className='courseName'>
              <label htmlFor='courseName'>Course Name:</label>
              <input
                type='text'
                placeholder='Enter Your course name'
                name='courseName'
                value={this.state.courseName}
                onChange={this.courseNameChangeHandler}
              />
            </div>
            <div className='courseDuration'>
              <label htmlFor='courseDuration'>courseDuration (in month)*</label>
              <input
                type='number'
                placeholder='choose your course duration'
                min='1'
                max='6'
                value={this.state.courseDuration}
                onChange={this.courseDurationChangeHandler}
              />
            </div>
            <div className='mentor'>
              <label htmlFor='mentor'>Mentor Name:</label>
              <input
                type='text'
                placeholder='Enter Your Mentor Name'
                value={this.state.mentor}
                name='mentor'
                onChange={this.changeMentorHandler}
              />
            </div>
            <button id='courseButton' type='submit'>
              ADD
            </button>
          </form>
        </center>
      </>
    );
  }
}
export default Course;
