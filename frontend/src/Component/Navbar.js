import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';

class NavbarComponent extends Component {
  render() {
    return (
      <nav className='navclass'>
        <ul>
          <li>
            <Link className='list' to='/about'>
              About us
            </Link>
          </li>
          <li>
            <Link className='list' to='/'>
              Home
            </Link>
          </li>
          <li>
            <Link className='list' to='/register'>
              Register
            </Link>
          </li>
          <li>
            <Link className='list' to='/login'>
              login
            </Link>
          </li>
          <li>
            <Link className='list' to='/courseavailaible'>
              Availaible Course
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default NavbarComponent;
