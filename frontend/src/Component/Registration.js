import React, { Component } from 'react';
//import { Jumbotron } from 'reactstrap';
import axios from 'axios';
import '../App.css';
import { Link } from 'react-router-dom';
//import { Redirect } from 'react-router-dom';

class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      contact: '',
      experience: '',
      subject: '',
      location: '',
      password: ''
    };
  }
  changeFirstNameHandler = e => {
    this.setState({
      firstName: e.target.value
    });
  };
  lastNameChangeHandler = e => {
    this.setState({
      lastName: e.target.value
    });
  };
  emailChangeHandler = e => {
    this.setState({ email: e.target.value });
  };
  changeContactHandler = e => {
    this.setState({
      contact: e.target.value
    });
  };
  changeExperienceHandler = e => {
    this.setState({
      experience: e.target.value
    });
  };
  changeSubjectHandler = e => {
    this.setState({
      subject: e.target.value
    });
  };
  locationChangeHandler = e => {
    this.setState({
      location: e.target.value
    });
  };
  passwordChangeHandler = e => {
    this.setState({
      password: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      contact: this.state.contact,
      experience: this.state.experience,
      subject: this.state.subject,
      location: this.state.location,
      password: this.state.password
    };

    axios.post('http://localhost:5000/api/teachers', data).then(res => {
      // console.log(res);
      if (res.statusText === 'OK') {
        alert('Thank You!! You are successfully Registered');
        this.props.history.push('/login');
      }
    });
  };

  render() {
    // const {firstName} = this.state
    //debugger
    return (
      <>
        <center></center>
        <center>
          <form onSubmit={this.handleSubmit} className='Registerform'>
            <div>
              <center>
                <h1>Please Register Here</h1>
                <hr className='line' />
              </center>
            </div>
            <div className='firstName'>
              <label htmlFor='firstnName'>First Name * :</label>
              <input
                type='text'
                placeholder='Enter Your First Name'
                name='firstName'
                value={this.state.firstName}
                onChange={this.changeFirstNameHandler}
              />
            </div>
            <div className='lastName'>
              <label htmlFor='lastName'>Last Name:</label>
              <input
                type='text'
                placeholder='Enter Your last name'
                name='lastName'
                value={this.state.lastName}
                onChange={this.lastNameChangeHandler}
              />
            </div>
            <div className='email'>
              <label htmlFor='email'>Email*</label>
              <input
                type='email'
                placeholder='Enter Your Email'
                value={this.state.email}
                onChange={this.emailChangeHandler}
              />
            </div>
            <div className='contact'>
              <label htmlFor='contact'>Contact Number :</label>
              <input
                type='text'
                placeholder='Enter Your contact Number'
                value={this.state.contact}
                name='contact'
                onChange={this.changeContactHandler}
              />
            </div>
            <div className='experience'>
              <label htmlFor='experience'>Experience*</label>
              <input
                type='number'
                placeholder='Choose your experience'
                min='0'
                max='40'
                value={this.state.experience}
                name='experience'
                onChange={this.changeExperienceHandler}
              />
            </div>
            <div className='subject*'>
              <label htmlFor='subject'>Subject*:</label>
              <select
                name=''
                id=''
                value={this.state.subject}
                onChange={this.changeSubjectHandler}
              >
                <option value='physics'>Physics</option>
                <option value='chemistry'>Chemistry</option>
                <option value='mathematics'>Mathematics</option>
              </select>
            </div>
            <br />
            <div className='location'>
              <label htmlFor=''>Location:</label>
              <input
                type='text'
                placeholder='Enter Your Location'
                value={this.state.location}
                onChange={this.locationChangeHandler}
              />
            </div>
            <div className='password'>
              <label htmlFor=''>Create Your Password*</label>
              <input
                type='password'
                placeholder='Create Your Password'
                value={this.state.password}
                onChange={this.passwordChangeHandler}
              />
            </div>

            <button id='registerButton' type='submit'>
              Register
            </button>
          </form>
          <p>
            Already have a account? <Link to='/login'>Login </Link>
          </p>
        </center>
      </>
    );
  }
}

export default Registration;
