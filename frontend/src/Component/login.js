import React from 'react';
import axios from 'axios';
import '../App.css';
import { Link } from 'react-router-dom';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }
  //debugger
  render() {
    const { email, password } = this.state;
    return (
      <center>
        <form onSubmit={this.handleSubmit} className='loginForm'>
          <div>
            <center>
              <h2>Enter Your Login Credential</h2>
              <hr className='line' />
            </center>
          </div>
          <label htmlFor='email'>Email</label>
          <input
            name='email'
            type='text'
            placeholder='Enter your email'
            value={email}
            onChange={this.handleChange}
          />
          <label htmlFor='email'>Password</label>
          <input
            name='password'
            type='password'
            placeholder='Enter your password'
            value={password}
            onChange={this.handleChange}
          />
          <button type='submit'>Login</button>
        </form>
        <p>
          Don't have account? <Link to='/register'>Register</Link>
        </p>
      </center>
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password
    };

    axios.post('http://localhost:5000/api/login', data).then(res => {
      console.log(res);
      alert(res.data);
      this.props.history.push('/courses');
    });
  };
}

export default Login;
