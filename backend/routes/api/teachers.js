const express = require('express');

const router = express.Router();

const { check, validationResult } = require('express-validator');

const Teacher = require('../../models/Teacher');

const bcrypt = require('bcryptjs');

//@route POST api/teachers
// desc registration of teacher with proper details
//access public

router.post(
  '/',
  [
    check('firstName', 'First Name is Required')
      .not()
      .isEmpty(),

    check('lastName', 'Last Name is Required')
      .not()
      .isEmpty(),

    check('email', 'Please Input a valid Email').isEmail(),

    check('contact', 'Please Provide a valid mobile Number').isMobilePhone(),

    check('experience', 'please select your experience')
      .not()
      .isEmpty(),

    check('subject', 'Please select Your Subject')
      .not()
      .isEmpty(),

    check('location', 'Enter Your Location')
      .not()
      .isEmpty(),

    check(
      'password',
      'Enter a strong password with atleast 8 character'
    ).isLength({ min: 8 })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors);
      return res.status(400).send(errors);
    }
    const {
      firstName,
      lastName,
      email,
      contact,
      experience,
      subject,
      location,
      password
    } = req.body;

    try {
      let user = await Teacher.findOne({ email });
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'User already Exist' }] });
      }
      //creating new teacher ... only create not save
      teacher = new Teacher({
        firstName,
        lastName,
        email,
        contact,
        experience,
        subject,
        location,
        password
      });
      //encrypting the password
      const salt = await bcrypt.genSalt(10);
      teacher.password = await bcrypt.hash(password, salt);
      await teacher.save();

      res.send('You are successfully registered');
    } catch (err) {
      console.log(err);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
