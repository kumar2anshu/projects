const express = require('express');
const router = express.Router();
const Teacher = require('../models/Teacher');
const Course = require('../models/Course'); //for courseapi
//get a list of teachers from the database

// router.get("/teachers",function(req,res,next){
//     res.send({type:"GET"});

// })

// router.delete("/teachers/:id",function(req,res,next){
//     res.send("This is get method");
//     res.end;

// })

router.post('/teachers', function(req, res, next) {
  //console.log(req.body);

  //we first want to load all the data according to
  // our model into database and than we want to fire some code that's why used then function
  //argument data inside the unction represents all the data which havve been saved .

  Teacher.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});

router.post('/login', (req, res) => {
  Teacher.findOne({ email: req.body.email }, (err, user) => {
    if (!user) res.json({ message: 'This Email is not registered with us.' });
    //password check....tough part
    user.comparePassword(req.body.password, (err, isMatch) => {
      if (!isMatch) return res.status(400).send('Wrong password');
      else return res.status(200).send('logged in successfully');
    });
  });
});

// router.put("/teachers/:id",function(req,res,next){
//     res.send("This is put method");
//     res.end;

// })

//for course api

router.post('/courses', function(req, res, next) {
  Course.create(req.body)
    .then(function(data) {
      res.send(data);
    })
    .catch(next);
});

//getting all courses
router.get('/courseavailaible', async (req, res) => {
  try {
    const allcourses = await Course.find({});
    res.send(allcourses);
  } catch (error) {
    res.status(500).send('Server Error');
  }
});

module.exports = router;
