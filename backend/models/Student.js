const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
let SALT = 10;

//create student schema and model
const studentSchema = new Schema({
  firstName: {
    type: String,
    required: [true, 'Name field is required']
  },
  lastName: {
    type: String
  },
  standarad: {
    type: String,
    required: [true, 'Standarad is required']
  },

  email: {
    type: String,
    required: [true, 'Email is mandatory']
  },
  contact: {
    type: String
  },

  location: {
    type: String
  },
  password: {
    type: String,
    required: [true, 'password is mandatory']
  }
});
//hashing password
studentSchema.pre('save', function(next) {
  var user = this;
  if (user.isModified('password')) {
    bcrypt.genSalt(SALT, function(err, salt) {
      if (err) return next(err);
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});
//comparing passwords
studentSchema.methods.comparePassword = function(
  candidatePassword,
  checkpassword
) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return checkpassword(err);
    checkpassword(null, isMatch);
  });
};

//CREATING MODEL
const Student = mongoose.model('students', studentSchema); //students is the name of collection
//aLSO  we want object in this collection "students" ...that object will be the instance of our schema which we have created above

//finally we want to export this model so that we can use it in outer file...specifically for our route
module.exports = Student;
