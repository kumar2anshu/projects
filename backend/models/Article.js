const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Article should be connected with the teacher
const ArticleSchema = new Schema({
  teacher: {
    type: Schema.Types.ObjectId,
    ref: 'teachers'
  },
  text: {
    type: String,
    required: true
  },
  articleName: {
    type: String,
    required: true
  },
  likes: [
    {
      student: {
        type: Schema.Types.ObjectId,
        ref: 'students'
      }
    }
  ],
  comments: [
    {
      student: {
        type: Schema.Types.ObjectId,
        ref: 'students'
      },
      text: {
        type: String,
        required: true
      },
      name: {
        type: String
      },
      date: {
        type: Date,
        default: Date.now
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now
  }
});
const Article = mongoose.model('articles', ArticleSchema);
module.exports = Article;
